# Written by: Ahmad Farid Kheradyar
# Date: 8/13/2020
# Purpose: this is an ordering Software created for Friend's Kebabs
# takeaway restaurant which employees can place costumer's order and print it
# and save all order's in the database
from tkinter import ttk
from tkinter import *
from ttkthemes import ThemedTk
from datetime import date
from tkinter import messagebox

global reviewRoot, found_id, get_current, totPrice, tot_revPrice, bak
global searchRoot
searchedOrders = []
Orders = []  # the dictionary used to send and the data to the file and
orderList = {"Food_type": {"type": "", "price": ""},
             "Meat": {"type": "", "price": ""},
             "Salad": {"type": "", "price": ""},
             "Sauces": {"type": "", "price": ""},
             "Extras": {"type": "", "price": ""},
             "baklava": {"type": "", "price": ""},
             "Chips": {"type": "", "price": ""},
             "Drink": {"type": "", "price": ""}}
countChips = 0
countExtra = 0
today = date.today()  # gets today's date and keeps in variable today
a = 0
x = False
y = False
sauces = ""
extra = ""
salad = ""
chipsQty = ""
saladCounter = 0
customerId = 1000
totalPrice = 0
# =====================================================================
# This Function gets all data from the Orders.txt file and makes a dictionary of
# the data as dict(Order). and this list used many times in the file
# =======================================================================
def get_orders():
    placedOrders = "Orders.txt"
    f = open(placedOrders, "r")  # open this file
    field = 0
    for line in f.readlines():  # read lines
        line = line.rstrip('\n')  # remove the carriage return /n at the end of each line
        if field == 0:
            Orders.append({})  # add a list item consisting of an empty dictionary
            Orders[-1]['ID'] = line
            print("id is = " + line)
            field += 1
        elif field == 1:
            Orders[-1]['Date'] = line
            field += 1
        elif field == 2:
            Orders[-1]['FoodType'] = line
            print("food is = " + line)
            field += 1
        elif field == 3:
            Orders[-1]['MeatType'] = line
            field += 1
        elif field == 4:
            Orders[-1]['Salad'] = line
            field += 1
        elif field == 5:
            Orders[-1]['Sauces'] = line
            field += 1
        elif field == 6:
            Orders[-1]['Extras'] = line
            field += 1
        elif field == 7:
            Orders[-1]['Baklava'] = line
            field += 1
        elif field == 8:
            Orders[-1]['Chips'] = line
            field += 1
        elif field == 9:
            Orders[-1]['Drinks'] = line
            field += 1
        elif field == 10:

            Orders[-1]['Tot_Price'] = line
            field += 1
        elif field == 11:
            Orders[-1]['Paid'] = line
            field = 0
            print(Orders[-1])
    f.close()


# ============================================================
# This function generates new ID and sets the id of new order
# and gives the date when the order placed
# =========================================================
def generate_id():
    global customerId

    if len(Orders) == 0:
        customerId = int(customerId)
        customerId = 100
        intId.set(str(customerId))
    else:
        customerId = int(Orders[-1]["ID"])
        customerId += 1
        intId.set(str(customerId))

    print("Curent ID is --> " + str(customerId))
    dt = today
    dtDate.set(str(dt))


def closing():              # this function can be called when  Id not found
    global searchRoot       # and error box showed
    searchRoot.destroy()


# ===============================================================================
# This function can be called when Search button clicked which first checks if any other
# window is open or not and closes other windows if open and then opens the search result window
# for searched id which allows the user to Edit the order or review it.
# =========================================================================
def search_id():
    global idResult, lblResultId, found_id, get_current, get_id
    global reviewRoot
    global searchRoot
    global x
    global y
    global foundId
    if y:  # closes the review button if it is open and user wants to open search window
        reviewRoot.destroy()
        y = False
    toGet = [x for x in txtSearch.get().split()]  # this is a list of search entry characters
    if len(toGet) == 0:     # checks if the something is entered in search entry
        messagebox.showwarning("Blank Search", "NULL! \nPlease enter an numeric ID and search")
    elif len(toGet) >= 0:
        try:                        # checking whether the entered value in search entry is integer
            int(txtSearch.get())
            if int(txtSearch.get()) >= 5000 or int(txtSearch.get()) <= 99:
                messagebox.showwarning("Character Length",       # message box to show the entered number is not valide
                                   "Too many OR less characters! \nPlease enter a valid numeric ID and search")
            else:
                try:
                    if searchRoot.state() == "normal":
                        searchRoot.focus()
                except:
                    searchRoot = Tk()  # creates the search window
                    searchRoot.title("Search")
                    searchFrame = ttk.Frame(searchRoot, padding="3 3 12 12")
                    searchFrame.grid(column=0, row=0, sticky=(N, W, E, S))
                    x = True
                    lblSearch = ttk.Label(searchFrame, text="Search Results", font=18, justify=CENTER)
                    lblSearch.grid(row=0, column=1, columnspan=2)
                    for id_pointer in range(len(Orders)):
                        get_current = id_pointer
                        print("current == " + str(get_current))
                        print("the id_pointer is : " + str(id_pointer) + "item ID " + Orders[id_pointer]["ID"])
                        if Orders[id_pointer]["ID"] == str(txtSearch.get()):
                            # print("the id_pointer is : " + str(id_pointer))
                            print("the found is : " + str(id_pointer) + "item ID " + Orders[id_pointer]["ID"])
                            # id should have been taken
                            lblSearchedId = ttk.Label(searchFrame, text="ID:", font=10)
                            lblSearchedId.grid(row=1, column=0, sticky=W)

                            lblResultId = ttk.Label(searchFrame, text="", font=10)
                            lblResultId.grid(row=1, column=1, sticky=E)
                            lblResultId.config(text=txtSearch.get())

                            serPrintBtn = ttk.Button(searchFrame, text="Review", command=review_order)
                            serPrintBtn.grid(column=0, row=2, sticky=W)

                            serEditBtn = ttk.Button(searchFrame, text="Edit", command=edit_order)
                            serEditBtn.grid(column=1, row=2, sticky=W)
                            break
                        elif get_current == (len(Orders) - 1):
                            messagebox.showwarning("ID type", "ID NOT FOUND !!!")
                        else:
                            continue

                    for child in searchFrame.winfo_children(): child.grid_configure(padx=5, pady=5)
                    searchRoot.mainloop()
        except ValueError:
            messagebox.showwarning("ID type", "Please search for numeric ID")


# ======================================================================
# This function opens the order's review windows which all items summary with
# the price of those are listed and two buttons gives the options to edit or print the order.
# =================================================================================.
def review_order():
    print("review Order Passed")
    global searchRoot, totPrice, get_current, tot_revPrice
    global orderList
    global x
    global y
    revR = 3
    global reviewRoot
    if x:
        searchRoot.destroy()
        x = False
    try:     # checks If review window is already open or not
        if reviewRoot.state() == "normal":
            reviewRoot.focus()
            get_current = len(Orders) - 1
    except:
        reviewRoot = Tk()  # if the review window is not opened it opens the window
        reviewRoot.title("Review")
        y = True
        get_current = len(Orders) - 1
        reviewFrame = ttk.Frame(reviewRoot, padding="3 3 12 12")
        reviewFrame.grid(column=0, row=0, sticky=(N, W, E, S))
        lblMan = ttk.Label(reviewFrame, text="Friend's Kebabs", font=16, justify=CENTER)
        lblMan.grid(column=0, row=0, columnspan=2)

        ttk.Label(reviewFrame, text="ID").grid(column=0, row=1, sticky=W)
        ttk.Label(reviewFrame, text=str(intId.get())).grid(column=1, row=1, sticky=W)
        ttk.Label(reviewFrame, text="Date").grid(column=0, row=2, sticky=W)
        ttk.Label(reviewFrame, text=str(dtDate.get())).grid(column=1, row=2, sticky=W)
        for (selText, selValue) in orderList.items():
            print(type(selText), selText, type(selValue), selValue)
            if selValue["type"] != "":
                ttk.Label(reviewFrame, text=str(selText + ":")).grid(column=0, row=revR, sticky=W)
                revR += 1
                for (foodOtp, optFoodPrice) in selValue.items():
                    ttk.Label(reviewFrame, text=selValue["type"]).grid(column=0, row=revR, sticky=W)
                    ttk.Label(reviewFrame, text=selValue["price"]).grid(column=1, row=revR, sticky=E)
                    selValue["price"] = ""
                    selValue["type"] = ""
            revR += 1

        lblTot = ttk.Label(reviewFrame, text="total")
        lblTot.grid(column=0, row=revR, sticky=W)
        totPrice = ttk.Label(reviewFrame, text=tot_revPrice)
        totPrice.grid(column=1, row=revR, sticky=E)
        revR += 1
        recPrintBtn = ttk.Button(reviewFrame, text="Print and New", command=print_order)
        recPrintBtn.grid(column=0, row=revR, sticky=W)
        recEditBtn = ttk.Button(reviewFrame, text="Edit", command=edit_order)
        recEditBtn.grid(column=1, row=revR, sticky=E)
        for child in reviewFrame.winfo_children(): child.grid_configure(padx=5, pady=5)

        reviewRoot.mainloop()  # packs the review window


# ===================================================================================
# this function can be called only in the review window which prints the order's summary from review window
# and closes all other windows and returns to the main window
# ===================================================================================
def print_order():
    print('hello')
    global reviewRoot, customerId
    reviewRoot.destroy()
    customerId += 1
    messagebox.showinfo("Print", "Printing order for the application is under maintenance")


# ======================================================================
# This function gets all selected extra items pack them in a list and
# count the price of those
# ======================================================================
def get_extra():
    global extra, countExtra, bak, totalPrice
    o = olives.get()
    ch = cheese.get()
    m = meat.get()
    j = jalapeno.get()
    if cmbBaklava.current() != 0:
        qty = int(cmbBaklava.get())
        baklavaQty.set(qty)
        bakPrice = qty * 5
        baklavaPrice.set(str(bakPrice))
        orderList["baklava"]["type"] = str(qty)
        orderList["baklava"]["price"] = str(bakPrice)
        totalPrice = totalPrice + bakPrice
    Extras = [o, ch, m, j]
    for e in Extras:
        if e != "":
            extra = str(e) + "," + str(extra)  # packing the selected items in the dictionary "orderList"
            orderList["Extras"]["type"] = extra
            print(e + ":" + extra)  # Start counting the prices
            countExtra += 2
            orderList["Extras"]["price"] = str(countExtra)
    totalPrice = totalPrice + countExtra
    priceTotal.set(str(totalPrice))
    extraPrice.set(str(countExtra) + "$")
    countExtra = 0
    extras.set(extra)
    extra = ""


# ============================================================
# This function gets and packs all selected salads
# ============================================================
def get_salad():
    global salad
    Salads = [str(lettuce.get()), str(onion.get()), str(tomatoSalad.get())]
    for sal in Salads:  # loops through all salads to check if selected
        if sal != "":
            salad = str(sal) + "," + str(salad)
            orderList["Salad"]["type"] = salad
            orderList["Salad"]["price"] = "$0"
            print(salad)
    saladsOptions.set(salad)
    salad = ""


# ============================================================
# This function gets and packs all selected sauces
# ============================================================
def get_sauces():
    global sauces
    g = garlic.get()
    Bbq = bbq.get()
    s = sour.get()
    sChi = sChili.get()
    hChi = hChili.get()
    Mayo = mayo.get()
    lj = lemon.get()
    t = tomato.get()
    h = hummus.get()
    Sauces = [g, Bbq, s, sChi, hChi, Mayo, lj, t, h]
    for s in Sauces:
        global sauces
        if s != "":
            sauces = str(s) + "," + str(sauces)
            orderList["Sauces"]["type"] = sauces
            orderList["Sauces"]["price"] = "$0"
            print(sauces)

    sauceOptions.set(sauces)
    sauces = ""


# ============================================================
# This function finds the selected food's price and labels it in front of the foodTypes
# ============================================================


def food_price(food_value):
    global totalPrice
    if food_value == "kebab":
        foodPrice.set(11)
    elif food_value == "kids":
        foodPrice.set(8)
    elif food_value == "container":
        foodPrice.set(15)
    elif food_value == "L.HSP":
        foodPrice.set(28)
    elif food_value == "M.HSP":
        foodPrice.set(15)
    elif food_value == "S.HSP":
        foodPrice.set(12)
    orderList["Food_type"]["price"] = str(foodPrice.get())   # adding the items to "orderList" dictionary
    priceTotal.set(str(totalPrice))


# ============================================================
# This function gets the selected amounts of every chips size
# packs them to send to the database and counts the price
# ============================================================


def select_chips():
    global totalPrice
    global chipsQty
    global countChips
    colChips = [cmbLchips.get(), cmbMchips.get(), cmbSchips.get()]  # gets the selected amount of every chips size
    for size in colChips:
        if int(size) != 0:
            chipsQty = str(size) + "," + str(chipsQty)
            if colChips.index(size) == 0:
                countChips = countChips + int(cmbLchips.get()) * 9
                orderList["Chips"]["price"] = str(countChips)
                Lqty = str(cmbLchips.get()) + "xL"
                orderList["Chips"]["type"] = Lqty
                chipsQty = Lqty
            if colChips.index(size) == 1:       # checks if medium chips selected and gets the quantity if selected
                Mqty = str(cmbMchips.get()) + "xM"
                countChips = countChips + int(cmbMchips.get()) * 7  #counting the price of amount of medium chips
                orderList["Chips"]["price"] = str(countChips)
                if orderList["Chips"]["type"] == "":
                    orderList["Chips"]["type"] = Mqty
                    chipsQty = Mqty
                else:
                    orderList["Chips"]["type"] = orderList["Chips"]["type"] + "," + Mqty
                    chipsQty = Mqty + "," + str(chipsQty)
            if colChips.index(size) == 2:
                Sqty = str(cmbSchips.get()) + "xS"
                countChips = countChips + int(cmbSchips.get()) * 5
                orderList["Chips"]["price"] = str(countChips)
                if orderList["Chips"]["type"] == "":
                    orderList["Chips"]["type"] = Sqty
                    chipsQty = Sqty
                else:
                    orderList["Chips"]["type"] = orderList["Chips"]["type"] + "," + Sqty
                    chipsQty = Sqty + "," + str(chipsQty)

            print(chipsQty)
    chipsPrice.set(str(countChips))
    totalPrice = totalPrice + countChips
    priceTotal.set(str(totalPrice))
    chipsOptions.set(str(chipsQty))
    countChips = 0
    chipsQty = ""


# ============================================================
# This function gets the selected amounts of drink, counts the price
# packs them to send to the database and
# ============================================================
def get_drink():
    global totalPrice
    dr = cmbDrink.get()
    drPrice = 0
    print(int(dr))
    if int(dr) != 0:
        drinkQty.set(str(dr))
        drPrice = int(dr) * 3
        orderList["Drink"]["type"] = str(dr)
        orderList["Drink"]["price"] = str(drPrice)
    drinkPrice.set(str(drPrice))
    totalPrice = totalPrice + drPrice
    priceTotal.set(str(totalPrice))


# ============================================================
# This function gets if the price is paid or not
# packs them to send it to the database and
# ============================================================
def check_paid():
    p = paidOk.get()
    if int(p) == 1:
        paid.set("Paid")
    else:
        paid.set("unpaid")


# ================================================================================
# This function can be called in search's result or review window.
# It closes all other window and returns to the main window which is
# already preselected with the items for a searched id
# =================================================================================
def edit_order():
    global searchRoot, get_current
    global reviewRoot
    global x
    global y
    if x:
        searchRoot.destroy()
        x = False
    if y:
        reviewRoot.destroy()
        y = False
    print("Edit Window")  # testing
    intId.set(Orders[get_current]["ID"])
    dtDate.set(Orders[get_current]["Date"])
    foodType.set(Orders[get_current]["FoodType"])
    meats.set(Orders[get_current]["MeatType"])
    fileSalad = list(Orders[get_current]["Salad"].split(","))
    for i in fileSalad:
        if i == "lettuce":
            lettuce.set("lettuce")
        if i == "onion":
            onion.set("onion")
        if i == "tomato":
            tomatoSalad.set("tomato")
    fileSauces = list(Orders[get_current]["Sauces"].split(","))
    for i in fileSauces:
        if i == "G":
            garlic.set("G")
        if i == "T":
            tomato.set("T")
        if i == "M":
            mayo.set("M")
        if i == "S":
            sour.set("S")
        if i == "HC":
            hChili.set("HC")
        if i == "SH":
            sChili.set("SH")
        if i == "LJ":
            lemon.set("LJ")
        if i == "BBQ":
            bbq.set("BBQ")
        if i == "H":
            hummus.set("H")
    fileExtra = list(Orders[get_current]["Extras"].split(","))
    for i in fileExtra:
        if i == "Cheese":
            cheese.set("Cheese")
        if i == "Olives":
            olives.set("Olives")
        if i == "Jalapeno":
            jalapeno.set(i)
        if i == "Meat":
            meat.set("Meat")
    if Orders[get_current]["Baklava"] == "":
        cmbBaklava.current(0)
    else:
        cmbBaklava.set(int(Orders[get_current]["Baklava"]))
    fileChips = list(Orders[get_current]["Chips"].split(","))
    for i in fileChips:
        for e in list(i):
            if e[-1] == "L":
                cmbLchips.current(int(e[0]))
        for e in list(i):
            if e[-1] == "M":
                cmbMchips.current(int(e[0]))
        for e in list(i):
            if e[-1] == "S":
                cmbSchips.current(int(e[0]))
    if Orders[get_current]["Drinks"] == "":
        cmbDrink.current(0)
    else:
        cmbDrink.current(int(Orders[get_current]["Drinks"]))
    priceTotal.set(Orders[get_current]["Tot_Price"])
    if str(Orders[get_current]["Paid"]) == "Paid":
        paidOk.set(1)
    else:
        paidOk.set(0)


# ---------------------------------------------------------------
# this function opens the data store file. gets all packs of selected items
# makes the fields for every item types and write the data to the file.
# ______________________________________________________________
def save_order():
    global customerId, totalPrice, foodPrice, totPrice, tot_revPrice
    global orderList
    get_extra()       # calling the functions when the save button is clicked
    check_paid()
    get_sauces()
    get_salad()
    select_chips()
    get_drink()
    print("Saved")
    totalPrice = int(totalPrice) + int(foodPrice.get())
    priceTotal.set(totalPrice)
    totPrice = priceTotal
    placedOrders = "Orders.txt"
    file = open(placedOrders, "a+")  # opening the data store file

    for pointer in range(len(Orders)):
        print(len(Orders))
        file.write('%s\n' % Orders[pointer]["ID"])
        file.write('%s\n' % Orders[pointer]["Date"])
        file.write('%s\n' % Orders[pointer]["FoodType"])
        file.write('%s\n' % Orders[pointer]["MeatType"])
        file.write('%s\n' % Orders[pointer]["Salad"])
        file.write('%s\n' % Orders[pointer]["Sauces"])
        file.write('%s\n' % Orders[pointer]["Extras"])
        file.write('%s\n' % Orders[pointer]["Baklava"])
        file.write('%s\n' % Orders[pointer]["Chips"])
        file.write('%s\n' % Orders[pointer]["Drinks"])
        file.write('%s\n' % Orders[pointer]["Tot_Price"])
        file.write('%s\n' % Orders[pointer]["Paid"])

    temp = intId.get()
    file.write('%s\n' % temp)

    intId.set(str(customerId))

    temp = dtDate.get()
    file.write('%s\n' % temp)
    dtDate.set('')

    temp = foodType.get()
    orderList["Food_type"]["type"] = temp
    file.write('%s\n' % temp)
    foodType.set('')

    temp = meats.get()
    orderList["Meat"]["type"] = temp
    orderList["Meat"]["price"] = "$0"
    file.write('%s\n' % temp)
    meats.set('')

    temp = saladsOptions.get()
    file.write('%s\n' % temp)
    saladsOptions.set('')

    temp = sauceOptions.get()
    file.write('%s\n' % temp)
    sauceOptions.set('')

    temp = extras.get()
    file.write('%s\n' % temp)
    extras.set('')

    temp = baklavaQty.get()
    file.write('%s\n' % temp)
    baklavaQty.set('')

    temp = chipsOptions.get()
    file.write('%s\n' % temp)
    chipsOptions.set('')

    temp = drinkQty.get()
    file.write('%s\n' % temp)
    drinkQty.set('')

    temp = priceTotal.get()
    file.write('%s\n' % temp)
    tot_revPrice = temp
    priceTotal.set('')
    totalPrice = 0
    temp = paid.get()
    file.write('%s\n' % temp)
    paid.set('')

    print(orderList)
    file.close()
    print("storeName")



# ========================================================================
# the main GUI window Starts form here
# =========================================================================
root = ThemedTk(theme="arc")  # the theme arc used for styling the widgets
root.title("Main")

mainframe = ttk.Frame(root, padding="3 3 12 12")
mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
root.columnconfigure(0, weight=1)
root.rowconfigure(0, weight=1)

style = ttk.Style()  # costume style for button and entry
style.configure('TButton', foreground='black', background="orange", relief="flat")
style.configure('TEntry', foreground='green', background="gray", border=8)

foodPrice = StringVar()
lblFoodPrice = ttk.Label(mainframe, textvariable=foodPrice).grid(row=1, column=5)
saladPrice = StringVar()
lblSaladPrice = ttk.Label(mainframe, textvariable=saladPrice).grid(row=5, column=5)
extraPrice = StringVar()
lblExtraPrice = ttk.Label(mainframe, textvariable=extraPrice).grid(row=9, column=5)
baklavaPrice = StringVar()
lblBaklava = ttk.Label(mainframe, textvariable=baklavaPrice).grid(row=11, column=5)
chipsPrice = StringVar()
lblChipsPrice = ttk.Label(mainframe, textvariable=chipsPrice).grid(row=12, column=5)
drinkPrice = StringVar()
lblDrinkPrice = ttk.Label(mainframe, textvariable=drinkPrice).grid(row=13, column=5)

# header starts from here
lblId = ttk.Label(mainframe, text="ID:")
lblId.grid(row=0, column=0, padx=5, sticky=E)
intId = StringVar()
txtID = ttk.Label(mainframe, textvariable=intId)
txtID.grid(row=0, column=1, sticky=E)

lblDate = ttk.Label(mainframe, text="Date:")
lblDate.grid(row=0, column=2, padx=5, sticky=E)
dtDate = StringVar()
txtDate = ttk.Label(mainframe, text=today)
txtDate.grid(row=0, column=3, sticky=W)
idResult = StringVar()
txtSearch = StringVar()
entSearch = ttk.Entry(mainframe, style="TEntry", textvariable=txtSearch)
entSearch.grid(row=0, column=4, padx=5, sticky=E)
btnSearch = ttk.Button(mainframe, text="Search", command=search_id)  # button for searching the id and
btnSearch.grid(row=0, column=5, sticky=E)  # opening the search result window
#    End of header
# ____________________________________________---
# FoodTypes selection radioButtons designs starts here
# --------------------------
lblFood = ttk.Label(mainframe, text="Food Type:")
lblFood.grid(row=1, column=0, sticky=W)

foodType = StringVar()

values = {"Kebab": "kebab", "KidsKebab": "kids", "Container": "container",
          "L.HSP": "L.HSP", "M.HSP": "M.HSP", "S.HSP": "S.HSP"}
b1 = 1
a1 = 1
for (text, value) in values.items():  # loops through values in foodTypes dictionary and creates radiobutton for
    fButton = ttk.Radiobutton(mainframe, variable=foodType, value=value, text=text,
                              command=lambda: food_price(foodType.get())).grid(row=a1, column=b1, sticky=W)
    b1 += 1
    if b1 == 4:
        a1 = 2
        b1 = 1
# ____________________________________________---
# Meats selection checkButtons designs starts here
# --------------------------------------------------
lblMeat = ttk.Label(mainframe, text="Meat:")
lblMeat.grid(row=3, column=0, sticky=W)
extras = StringVar()
meats = StringVar()
ValueMeats = {"Lamb": "lamb", "Chicken": "chi", "Mixed": "mix", "Felafel": "felafel"}
b = 3
a = 1
for (text, value) in ValueMeats.items():  # loops through ValueMeats dictionary and creates radiobutton for every items
    mOptions = ttk.Radiobutton(mainframe, variable=meats, value=value, text=text).grid(row=b, column=a, sticky=W)
    a += 1
    if a == 4:
        b = 4
        a = 1
print(meats.get())
# ___________________
# Salad selection checkButtons designs starts here
# --------------------------
lblSalads = ttk.Label(mainframe, text="Salads:")
lblSalads.grid(row=5, column=0, sticky=W)
saladsOptions = StringVar()
lettuce = StringVar()
chkLettuce = ttk.Checkbutton(mainframe, text="Lettuce", variable=lettuce, onvalue="lettuce", offvalue="")
chkLettuce.grid(row=5, column=1, sticky=W)
tomatoSalad = StringVar()
chkTomato = ttk.Checkbutton(mainframe, text="Tomato", variable=tomatoSalad, onvalue="tomato", offvalue="")
chkTomato.grid(row=5, column=2, sticky=W)

onion = StringVar()
chkOnion = ttk.Checkbutton(mainframe, text="Onion", variable=onion, onvalue="onion", offvalue="")
chkOnion.grid(row=5, column=3, sticky=W)

# _____________________________________________
# Sauces selection checkButtons designs starts here
# -----------------------------------------------
sauceOptions = StringVar()
lblSauces = ttk.Label(mainframe, text="Sauces:")
lblSauces.grid(row=6, column=0, sticky=W)

garlic = StringVar()
chkGarlic = ttk.Checkbutton(mainframe, text="Garlic", variable=garlic, onvalue="G", offvalue="")
chkGarlic.grid(row=6, column=1, sticky=W)

bbq = StringVar()
chkBbq = ttk.Checkbutton(mainframe, text="BBQ", variable=bbq, onvalue="BBQ", offvalue="")
chkBbq.grid(row=6, column=2, sticky=W)

tomato = StringVar()
chkTsauce = ttk.Checkbutton(mainframe, text="Tomato", variable=tomato, onvalue="T", offvalue="")
chkTsauce.grid(row=6, column=3, sticky=W)

hChili = StringVar()
chkHchili = ttk.Checkbutton(mainframe, text="H.Chili", variable=hChili, onvalue="HC", offvalue="")
chkHchili.grid(row=7, column=1, sticky=W)

sChili = StringVar()
chkSchili = ttk.Checkbutton(mainframe, text="S.Chili", variable=sChili, onvalue="SH", offvalue="")
chkSchili.grid(row=7, column=2, sticky=W)

mayo = StringVar()
chkMayo = ttk.Checkbutton(mainframe, text="Mayo", variable=mayo, onvalue="M", offvalue="")
chkMayo.grid(row=7, column=3, sticky=W)

hummus = StringVar()
chkHummus = ttk.Checkbutton(mainframe, text="Hummus", variable=hummus, onvalue="H", offvalue="")
chkHummus.grid(row=8, column=1, sticky=W)

sour = StringVar()
chkSour = ttk.Checkbutton(mainframe, text="Sour Cream", variable=sour, onvalue="S", offvalue="")
chkSour.grid(row=8, column=2, sticky=W)

lemon = StringVar()
chkLjuice = ttk.Checkbutton(mainframe, text="Lemon Juice", variable=lemon, onvalue="LJ", offvalue="")
chkLjuice.grid(row=8, column=3, sticky=W)

# ___________________
# Extras selection checkbuttons designs starts here
# --------------------------
lblExtras = ttk.Label(mainframe, text="Extras:")
lblExtras.grid(row=9, column=0, sticky=W)

cheese = StringVar()
chkCheese = ttk.Checkbutton(mainframe, text="Cheese", variable=cheese, onvalue="Cheese", offvalue="")
chkCheese.grid(row=9, column=1, sticky=W)
jalapeno = StringVar()
chkJalapeno = ttk.Checkbutton(mainframe, text="Jalapenos", variable=jalapeno, onvalue="Jalapeno", offvalue="")
chkJalapeno.grid(row=9, column=2, sticky=W)
olives = StringVar()
chkOlives = ttk.Checkbutton(mainframe, text="Olives", variable=olives, onvalue="Olives", offvalue="")
chkOlives.grid(row=9, column=3, sticky=W)
meat = StringVar()
chkMeat = ttk.Checkbutton(mainframe, text="Meat", variable=meat, onvalue="Meat", offvalue="")
chkMeat.grid(row=10, column=1, sticky=W)

baklavaQty = StringVar()
lblBaklava = ttk.Label(mainframe, text="Baklava").grid(row=11, column=0, sticky=W)
cmbBaklava = ttk.Combobox(mainframe, width=4, text="Baklava")  # combobox for Baklava
cmbBaklava["values"] = ("0", "1", "2", "3", "4")
cmbBaklava.current(0)
cmbBaklava.grid(row=11, column=1, sticky=E)
# ___________________
# Chips selection ComboBoxes designs starts here
# --------------------------
chipsOptions = StringVar()
lblChips = ttk.Label(mainframe, text="Chips:")
lblChips.grid(row=12, column=0, sticky=W)

lchips = StringVar()
lblLchips = ttk.Label(mainframe, text="large").grid(row=12, column=1, sticky=W)
cmbLchips = ttk.Combobox(mainframe, width=4)  # combobox for large chips
cmbLchips["values"] = ("0", "1", "2", "3", "4")
cmbLchips.current(0)
cmbLchips.grid(row=12, column=1, sticky=E)

mChips = StringVar()
lblMchips = ttk.Label(mainframe, text="medium").grid(row=12, column=2, sticky=W)
cmbMchips = ttk.Combobox(mainframe, width=4)  # combobox for Medium chips
cmbMchips["values"] = ("0", "1", "2", "3", "4")
cmbMchips.current(0)
cmbMchips.grid(row=12, column=2, sticky=E)

sChips = StringVar()
lblSchips = ttk.Label(mainframe, text="small").grid(row=12, column=3, sticky=W)
cmbSchips = ttk.Combobox(mainframe, width=5)  # combobox for small chips
cmbSchips["values"] = ("0", "1", "2", "3", "4")
cmbSchips.current(0)
cmbSchips.grid(row=12, column=3, sticky=E)
# ___________________
# drink selection ComboBox designs starts here
# --------------------------
lblDrink = ttk.Label(mainframe, text="Drink:")
lblDrink.grid(row=13, column=0, sticky=W)
drinkQty = StringVar()
cmbDrink = ttk.Combobox(mainframe, width=9, text="Drink")
cmbDrink["values"] = ("0", "1", "2", "3", "4", "5")
cmbDrink.current(0)
cmbDrink.grid(row=13, column=1, sticky=W)

NewItemButton = ttk.Button(mainframe, text="Add More Items")  # button for adding more items
NewItemButton.grid(row=14, column=2)

priceTotal = StringVar()
lblTotal = ttk.Label(mainframe, text="Total Price: ")
lblTotal.grid(row=14, column=4, sticky=E)
txtTotal = ttk.Label(mainframe, text="$")  # total price label
txtTotal.grid(row=14, column=5)

paid = StringVar()
paidOk = IntVar()
chkPaid = ttk.Checkbutton(mainframe, text="Paid", variable=paidOk)  # paid checkbutton
chkPaid.grid(row=15, column=5, sticky=W)

btnSave = ttk.Button(mainframe, text="Save", command=save_order)  # button for save data to data store
btnSave.grid(row=15, column=1, pady=14, padx=6)

btnPrint = ttk.Button(mainframe, text="Review", command=review_order)  # button for opening review order window
btnPrint.grid(row=15, column=3)

for child in mainframe.winfo_children(): child.grid_configure(padx=5, pady=5)

get_orders()
generate_id()

# End of the main window
root.mainloop()
